{
  description = "Rust Development Overlay";

  inputs = {
    nixpkgs.url      = "github:nixos/nixpkgs/nixos-unstable";
    naersk = { url = "github:nix-community/naersk"; inputs.nixpkgs.follows = "nixpkgs"; };
    flake-utils.url  = "github:numtide/flake-utils";

    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, fenix, flake-utils, naersk, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ fenix.overlays.default ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };

        toolchain = (pkgs.fenix.toolchainOf {
          channel = "nightly";
          date    = "2022-10-20";
          sha256  = "sha256-drqeMQnMg2snYs7e2P2I6cH0EVjTxeJGLTvn0HW8mqA=";
        }).withComponents [
          "cargo" "rustc" "clippy" "rustfmt" "rust-src"
        ];

        naersk-lib = naersk.lib."${system}".override {
          rustc = toolchain;
          cargo = toolchain;
        };

        buildInputs = with pkgs; [
          openssl
          pkgconfig
          exa
          fd
          toolchain
          valgrind
          massif-visualizer

          # deps for eframe
          glib
          pango
          gdk-pixbuf
          atk
          gtk3
          libGL
          libglvnd
          freetype
          vulkan-headers
          vulkan-loader
          vulkan-tools
          wayland
          libxkbcommon
          ffmpeg_5-full
          fontconfig
          expat
        ];

        nativeBuildInputs = with pkgs; [ autoPatchelfHook wrapGAppsHook makeWrapper ];
        ldLibPath = pkgs.lib.makeLibraryPath buildInputs;

        proton_ovr = builtins.readFile ./proton-steam-comptime.txt;
      in
        rec {
          packages = {
            kspacker = naersk-lib.buildPackage {
              pname = "kspacker";
              root = ./.;
              buildInputs = buildInputs;
              nativeBuildInputs = nativeBuildInputs;
              postInstall = ''
              wrapProgram $out/bin/kspacker --prefix LD_LIBRARY_PATH : ${ldLibPath}
              '';

            };
            kspacker-dev = naersk-lib.buildPackage {
              pname = "kspacker";
              root = ./.;
              buildInputs = buildInputs;
              nativeBuildInputs = nativeBuildInputs;
              release = false;
              cargoBuildOptions = options: options ++ ["--features" "proton-steam-comptime"];
              PROTON_PATH_OVR = proton_ovr;

              postInstall = ''
              wrapProgram $out/bin/kspacker --prefix LD_LIBRARY_PATH : ${ldLibPath}
              '';
            };
          };
          defaultPackage = packages.kspacker;

          apps.kspacker = flake-utils.lib.mkApp {
            drv = packages.kspacker;
          };
          defaultApp = apps.kspacker;

          devShell = pkgs.mkShell {
            buildInputs = buildInputs ++ [ pkgs.rust-analyzer-nightly ];
            nativeBuildInputs = nativeBuildInputs;

            PROTON_PATH_OVR = proton_ovr;

            shellHook = ''
            echo "Loaded devshell"
            export LD_LIBRARY_PATH="${pkgs.lib.makeLibraryPath buildInputs}:$LD_LIBRARY_PATH"
          '';
          };
        }
    );
}
